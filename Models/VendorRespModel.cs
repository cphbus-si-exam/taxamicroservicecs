namespace Models
{
    public class VendorRespModel
    {
        public string eventid { get; set; }
        public string description { get; set; }
        public string additional_info { get; set; }
        public bool status { get; set; }
        public string request_type { get; set; }
    }
}